import { initializeApp } from "firebase/app";
import { getFirestore, collection, getDocs } from "firebase/firestore";
import StudentStore from "./stores/store";
// Follow this pattern to import other Firebase services
// import { } from 'firebase/<service>';

// TODO: Replace the following with your app's Firebase project configuration
const firebaseConfig = {
  apiKey: "AIzaSyA7ywDKT-OGBy5Q9VxyVLMgHOAuprjCD7k",
  authDomain: "crud-6f0ab.firebaseapp.com",
  projectId: "crud-6f0ab",
  storageBucket: "crud-6f0ab.appspot.com",
  messagingSenderId: "364565696014",
  appId: "1:364565696014:web:0ce2b5c9754bc8d72cf65f",
  measurementId: "G-GWC8XWL0DW",
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

// Get a list of cities from your database
const getAll = async ()=> {
  const querySnapshot = await getDocs(collection(db, "estudiantes"));
  querySnapshot.forEach((doc) => {
    console.log(`${doc.id} => ${doc.data().cost}`);
  });
  const studentList = querySnapshot.docs.map(doc => doc.data());
  StudentStore.data.students = studentList

  console.log(studentList);
  return studentList;
  
}

export default getAll