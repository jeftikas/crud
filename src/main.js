import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import vuetify from './plugins/vuetify'
// import VueCompositionAPI from '@vue/composition-api'
import { createPinia, PiniaVuePlugin } from 'pinia'

Vue.use(PiniaVuePlugin)
const pinia = createPinia()

Vue.config.productionTip = false

// const pinia = createPinia()
// Vue.use(PiniaVuePlugin)

// Vue.use(VueCompositionAPI)


new Vue({
  pinia,
  vuetify,
  render: h => h(App)
}).$mount('#app')


// Vue.use(VueCompositionAPI)
